package ru.tsc.kitaev.tm.api.service;

import ru.tsc.kitaev.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
