package ru.tsc.kitaev.tm.api.entity;

import ru.tsc.kitaev.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
