package ru.tsc.kitaev.tm.exception.entity;

import ru.tsc.kitaev.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
