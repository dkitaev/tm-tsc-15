package ru.tsc.kitaev.tm.exception.empty;

import ru.tsc.kitaev.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error. Name is empty.");
    }

}
